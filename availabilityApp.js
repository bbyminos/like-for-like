var config = require('./server/config/config');
var fs = require('fs');
var path = require('path');
var request = require('request');

var appId = 'CoreBlue';
var certFile = path.resolve(__dirname, 'server/services/ssl/cert.pem');
var keyFile = path.resolve(__dirname, 'server/services/ssl/key.pem');

var options = {
    strictSSL: false,
    headers: {
        'Application-ID': appId // need to get our own id
    },
    cert: fs.readFileSync(certFile),
    key: fs.readFileSync(keyFile),
    passphrase: 'dude'
};

options.url = `${config.availabilityServiceUrl}5458583`;
options.headers.SellingStoreId = `BBY_281`;

// first, check the fulfillment options for this sku
request(options, function (error, res, body) {
    console.log(res);
});

//for(i = 0; i < 30; i++) {
//    var bby = require('bestbuy')('wvsmnrnnfus23u99thfexpy6');
//    bby.stores(`area(${zipCode},25)&storeType=*)+products(sku=${sku}`)
//        .then(function (res) {
//            if (res.statusCode === 403) {
//                throw new Error('allowed queries per second has been exceeded');
//            }
//
//            if (res.error) {
//                throw new Error(res)
//            }
//            console.log(`${res.stores.length} stores within 25 miles of zip code ${zipCode} have availability for sku ${sku}`);
//        })
//        .catch(function (err) {
//            console.warn(err);
//        });
//}

//var options = {
//    key: 'wvsmnrnnfus23u99thfexpy6',
//    url: 'https://api.bestbuy.com/v1/',
//    debug: false,
//};


//process.env.HTTPS_PROXY = 'http://delicious-pox-secondarymarkets.cdc.bestbuy.com:3128';
//var sku = 8274303;
////var sku = 8824329;
//var zipCode = 55423;
//var request = require('request');
//var _ = require('lodash');
//var storeService = require('./server/services/storeService');
//var availabilityService = require('./server/services/availabilityService');
//var log = require('./server/config/log');
//
//availabilityService.getAvailabilityFromOms(sku, zipCode)
//    .then((result) => {
//       console.log(result);
//    })
//    .catch((err) => {
//        throw err;
//    })






//var options = {
//    url: 'http://ptl01omsap02a:30280/yantra/webresources/fulfillmentOptions/api/v1/itemInquiry/' + sku,
//    headers: {
//        'Application-ID': 'CoreBlue'
//    }
//};
//
//var store_chain;
//
//storeService.getClosestStores(zipCode)
//    .then((result) => {
//
//        return Promise.map(result, (bbystore) => {
//            //foreach bbystore in result do something
//
//            options.headers.SellingStoreId = bbystore;
//
//            request(options, function (error, res, body) {
//
//                if (!error && res.statusCode == 200) {
//                    var fulfillments = parseSafe(body).fulfillmentOptions;
//                    if(fulfillments.isDeliveryAllowed == "Y") {
//
//                        store_chain += bbystore;
//
//                        options.url = `http://ptl01omsap02a:30280/yantra/webresources/fulfillmentOptions/api/v1/itemInquiry/${sku}/del/?zipCode=${zipCode}`;
//                        request(options, function (error, res, body) {
//                            if (!error && res.statusCode == 200) {
//                                console.log('all good here');  // we have availability
//                                console.log(store_chain);
//                            }
//                            else {
//                                console.log(options)
//                                console.log(body);
//                            }
//                        });
//                    }
//                }
//                else{
//                    console.log(`statusCode: ${res.statusCode}`)
//                    console.log(body);
//                }
//            });
//        });
//
//
//    });

// need to get the closest stores to a customer zipcode then...
// get all available fulfillment options using just the itemInquiry
// if isDeliveryAllowed is yes, then query for the delivery
//  if no, then do a spu lookup using each store from step 1.

//request(options, function (error, res, body) {
//   if (!error && res.statusCode == 200) {
//       var fulfillments = parseSafe(body).fulfillmentOptions;
//       if(fulfillments.isDeliveryAllowed == "Y") {
//
//           options.url = options.url + `/del/?zipCode=${zipCode}`;
//           request(options, function (error, res, body) {
//               if (!error && res.statusCode == 200) {
//                   console.log('all good here');
//               }
//               else {
//                   console.log(body);
//               }
//           });
//       }
//
//       //console.log(parseSafe(body).fulfillmentOptions.isDeliveryAllowed)
//   }
//   else{
//       console.log(`statusCode: ${res.statusCode}`)
//       console.log(body);
//   }
//});

function parseSafe(str) {
    return _.attempt(JSON.parse.bind(null, str));
}






//
//bby.availability(4312001, [611, 482])
//    .then(function(data){
//        //console.log('Stores carrying %s: %d', data.products[0].name, data.products[0].stores.length);
//        //console.log(data);
//    })
//    .catch(function(err){
//        console.warn(err);
//    });



//var options = {
//   host: 'www.bestbuy.com',
//   path: '/productfulfillment/api/1.0/storeAvailability',
//   method: 'POST',
//   headers: {
//      'Content-Type': 'application/json',
//      'Content-Length': Buffer.byteLength(JSON.stringify(data))
//   }
//};
//
//var req = http.request(options, function(res) {
//   res.setEncoding('utf-8');
//   res.on('data', function(chunk) {
//      console.log("Response:" + chunk);
//   })
//});
//
//req.write(JSON.stringify(data));
//req.end();


//
//curl.request(options, function(err, result) {
//   console.log(result);
//})



//var options = {
//   uri: 'http://www.bestbuy.com/productfulfillment/api/1.0/storeAvailability',
//   method: 'POST',
//   json: {"skus":[{"skuId":4358218,"quantity":1}],"zipCode":"55404","customerUuid":null}
//};
//
//request(options, function (error, response, body) {
//   if (!error && response.statusCode == 200) {
//      console.log(body)
//   }
//   else{
//      console.log(body);
//   }
//});
