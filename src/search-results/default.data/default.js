var helpers = require("./_helpers");

module.exports = exports = function (readDataFile) {
  var data = readDataFile("./_default-data.yml");
  
  helpers.randomizeImages(data);
  
  return data;
};
