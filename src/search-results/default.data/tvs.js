var helpers = require("./_helpers");

module.exports = exports = function (readDataFile) {
  var data = readDataFile("./_tvs.yml");

  helpers.randomizeImages(data, 2);

  return data;
};
