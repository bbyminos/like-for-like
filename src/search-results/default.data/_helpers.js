var util = require("util");

function lookForImageAndRandomize(value, imageName){
  for(var p in value){
    if(p === "image" && value[p].url !== undefined) {
      value[p].url = setProductImage(imageName);
      continue;
    }
    if(util.isString(value[p])) continue;
    lookForImageAndRandomize(value[p], imageName);
  }
}

function setProductImage(value){
  value = value || Math.floor(Math.random() * 10) + 1;
  return "/images/" + value + ".jpg";
}

exports.randomizeImages = lookForImageAndRandomize;
