var helpers = require("./_helpers");

module.exports = exports = function (readDataFile) {
  var data = readDataFile("./_laptops.yml");

  helpers.randomizeImages(data, 3);

  return data;
};
