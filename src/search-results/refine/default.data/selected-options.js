module.exports = exports = function (readDataFile) {
  var data = readDataFile("./default.yml");
  
  function inputNameFilter(name) {
    return function (searchGroup) {
      return searchGroup.input.name === name;
    }
  }
  
  function addSelectedOption(inputName, inputValue) {
    var filtered = data.searchGroups.filter(inputNameFilter(inputName));
    if (!filtered || !filtered.length === 1) return;
    var searchGroup = filtered[0];
    searchGroup.input.values = searchGroup.input.values || [];
    searchGroup.input.values.push(inputValue);
  }
  
  addSelectedOption("brand", "Samsung");
  addSelectedOption("brand", "Sony");
  
  return data;
};
