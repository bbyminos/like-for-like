var app = require("express")();
var fs = require("fs");
var path = require("path");
var urls = require("./build/urls");
var util = require("util");
var changeCase = require("change-case");
var handlebars = require("handlebars");
var http = require("http");
var basePath = path.resolve(process.cwd(), "./out");

// body parser middleware
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

var multer = require('multer');
app.use(multer()); // for parsing multipart/form-data

// static middleware
var staticMiddleware = require("serve-static");

staticMiddleware.mime.define({
  "application/lynx+json": ["lnx"],
  "application/lynx-spec+json": ["lnxs"]
});

function respondWithError(err, res) {
  console.error(err);
  res.set("content-type", "text/plain");
  res.status(500);
  res.send("An unexpected error occurred.");
}

function respondWithLynx(templateFile, data, res) {
  function cb(err, contents) {
    if (err) {
      return respondWithError(err, res);
    }
    
    try {
      var template = handlebars.compile(contents.toString(), { compat: true });
      var content = template(data);
      res.set("content-type", "application/lynx+json");
      res.status(200);
      res.send(content);
    }
    catch (err) {
      respondWithError(err, res);
    }
  }
  
  templateFile = path.resolve(basePath, templateFile);
  fs.readFile(templateFile, cb);
}

function getIndex(req, res) {
  getIndexData(req, res, function (data) {
    respondWithLynx("./default.mustache", data, res);
  });
}

function getIndexData(req, res, cb) {
  var data = {
    urls: urls.data,
    sku: {
      value: "",
      requiredState: "unknown",
      textState: "unknown"
    },
    zipCode: {
      value: "",
      requiredState: "unknown",
      textState: "unknown"
    }
  };
  
  cb(data);
}

function getSearchResults(req, res) {
  getSearchResultsData(req, res, function (data) {
    respondWithLynx("./search-results/default.mustache", data, res);
  });
}

function toDataString(str) {
  if (!str) return str;
  var result = JSON.stringify(str);
  return result.substring(1, result.length - 1);
}

function esProductToData(esProduct) {
  var data = {
    attributes: []
  };
  
  data.shortDescription = toDataString(esProduct.names.title);
  data.longDescription = "";
  
  data.image = {};
  if (esProduct.images) {  
    var esImage = esProduct.images.find(function (img) {
      return img.rel === "Front_Standard";
    });
    data.image.url = esImage.href;
    data.image.height = +esImage.height;
    data.image.width = +esImage.width;
    
    if (data.image.width > 150) {
      data.image.height = (data.image.height / data.image.width) * 150;
      data.image.width = 150;
    }
  }
  else {
    data.image.url = "https://placeholdit.imgix.net/~text?txtsize=18&txt=Image+Not+Available&w=150&h=100&txttrack=0";
    data.image.height = 100;
    data.image.width = 150;
  }
  
  data.attributes.push({ label: "SKU", text: esProduct.skuId });
  data.attributes.push({ label: "Model", text: toDataString(esProduct.manufacturer.modelNumber) });
  
  return data;
}

function dumpAttributes(esProduct) {
  // for (var key in esProduct) {
  //   if (util.isObject(esProduct[key])) continue;
  //   var out = [];
  //   out.push("- label: ");
  //   out.push(changeCase.title(key));
  //   out.push("\n  text: ");
  //   out.push(esProduct[key]);
  //   console.log(out.join(""));
  // }
  // 
  // console.log("-------------------------------------");
  // 
  // esProduct.specifications.forEach(function (specification) {
  //   var out = [];
  //   out.push("- label: ");
  //   out.push(specification.displayName);
  //   out.push("\n  text: ");
  //   out.push(specification.values[0]);
  //   console.log(out.join(""));
  // });
}

function getSearchResultsData(req, res, cb) {
  var sku = req.query.sku;
  
  var data = {
    urls: urls.data,
    requestedProduct: null,
    matchingProducts: []
  };
  
  function handleResponse(httpResponse) {
    var buf = [];

    httpResponse.on("data", function (chunk) {
      buf.push(new Buffer(chunk));
    });

    httpResponse.on("error", function (err) {
      respondWithError(err, res);
    });

    httpResponse.on("end", function () {
      try {
        if (buf.length > 0) {
          buf = Buffer.concat(buf);
          var esResult = JSON.parse(buf.toString());
          data.requestedProduct = esProductToData(esResult.sku);
          esResult.similarSkus.forEach(function (item) {
            data.matchingProducts.push(esProductToData(item));
          });
          
          dumpAttributes(esResult.similarSkus[0]);
        }
        
        cb(data);
      } catch (err) {
        respondWithError(err, res);
      }
    });
  }
  
  var searchUrl = "http://pll02dpawb004.na.bestbuy.com:8080/like-for-like/" + sku;
  http.get(searchUrl, handleResponse)
    .on("error", function (err) {
      respondWithError(err, res);
    });
}

function getComparison(req, res) {
  getComparisonData(req, res, function (data) {
    respondWithLynx("./comparison/default.mustache", data, res);
  });
}

function getComparisonData(req, res, cb) {
  var data = {
    urls: urls.data,
    "results": [
      {
        "isHeader": true,
        "header": null,
        "original": {
          "title": "Original",
          "image": {
            "url": "http://placehold.it/100x100",
            "height": 73,
            "width": 111
          }
        },
        "comparison": {
          "title": "Comparison",
          "image": {
            "url": "http://placehold.it/100x100",
            "height": 73,
            "width": 111
          }
        }
      },
      {
        "header": "Resolution",
        "original": "1080i",
        "comparison": "1080p"
      },
      {
        "header": "Size",
        "original": "40\\\"",
        "comparison": "43\\\""
      },
      {
        "header": "Attribute 3",
        "original": "Orig 3",
        "comparison": "Compare 3"
      },
      {
        "header": "Attribute 4",
        "original": "Orig 4",
        "comparison": "Compare 4"
      },
      {
        "header": "Resolution",
        "original": "1080i",
        "comparison": "1080p"
      },
      {
        "header": "Size",
        "original": "40\\\"",
        "comparison": "43\\\""
      },
      {
        "header": "Attribute 3",
        "original": "Orig 3",
        "comparison": "Compare 3"
      },
      {
        "header": "Attribute 4",
        "original": "Orig 4",
        "comparison": "Compare 4"
      },
      {
        "header": "Attribute 5",
        "original": "Orig 5",
        "comparison": "Compare 5"
      },
      {
        "header": "Resolution",
        "original": "1080i",
        "comparison": "1080p"
      },
      {
        "header": "Size",
        "original": "40\\\"",
        "comparison": "43\\\""
      },
      {
        "header": "Attribute 3",
        "original": "Orig 3",
        "comparison": "Compare 3"
      },
      {
        "header": "Attribute 4",
        "original": "Orig 4",
        "comparison": "Compare 4"
      },
      {
        "header": "Attribute 5",
        "original": "Orig 5",
        "comparison": "Compare 5"
      }
    ]
  };
  
  cb(data);
}

app.get("/", getIndex);
app.get("/search-results", getSearchResults);
app.get("/comparison", getComparison);

app.use(staticMiddleware("./out", { index: ["default.lnx", "index.html" ] }));

var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
  var host = server.address().address;
  console.log("The Like for Like application is listening at http://%s:%s", host, port);
});
