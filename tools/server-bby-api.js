var app = require("express")();
var fs = require("fs");
var path = require("path");
var urls = require("./build/urls");
var handlebars = require("handlebars");
var bby = require("bestbuy")("qnpprbvjzj6zw2ddba35wpjm");
var basePath = path.resolve(process.cwd(), "./out");

// body parser middleware
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

var multer = require('multer');
app.use(multer()); // for parsing multipart/form-data

// static middleware
var staticMiddleware = require("serve-static");

staticMiddleware.mime.define({
  "application/lynx+json": ["lnx"],
  "application/lynx-spec+json": ["lnxs"]
});

function respondWithError(err, res) {
  console.error(err);
  res.set("content-type", "text/plain");
  res.status(500);
  res.send(err.stack);
}

function respondWithLynx(templateFile, data, res) {
  function cb(err, contents) {
    if (err) {
      return respondWithError(err, res);
    }
    
    try {
      var template = handlebars.compile(contents.toString(), { compat: true });
      var content = template(data);
      res.set("content-type", "application/lynx+json");
      res.status(200);
      res.send(content);
    }
    catch (err) {
      respondWithError(err, res);
    }
  }
  
  templateFile = path.resolve(basePath, templateFile);
  fs.readFile(templateFile, cb);
}

function getIndex(req, res) {
  getIndexData(req, res, function (data) {
    respondWithLynx("./default.mustache", data, res);
  });
}

function getIndexData(req, res, cb) {
  var data = {
    urls: urls.data,
    sku: {
      value: "",
      requiredState: "unknown",
      textState: "unknown"
    },
    zipCode: {
      value: "",
      requiredState: "unknown",
      textState: "unknown"
    }
  };
  
  cb(data);
}

function getSearchResults(req, res) {
  getSearchResultsData(req, res, function (data) {
    respondWithLynx("./search-results/default.mustache", data, res);
  });
}

function toDataString(str) {
  if (!str) return str;
  var result = JSON.stringify(str);
  return result.substring(1, result.length - 1);
}

function bbyProductToData(bbyProduct) {
  var data = {
    attributes: []
  };
  
  data.shortDescription = toDataString(bbyProduct.name);
  data.longDescription = "";
  
  data.image = {};
  data.image.url = bbyProduct.image;
  data.image.height = 72;
  data.image.width = 125;
  
  data.attributes.push({ label: "SKU", text: bbyProduct.sku });
  data.attributes.push({ label: "Model", text: toDataString(bbyProduct.modelNumber) });
  
  return data;
}

function getSearchResultsData(req, res, cb) {
  var sku = req.query.sku;
  
  var data = {
    urls: urls.data,
    requestedProduct: null,
    matchingProducts: []
  };
  
  function handleFailure(err) {
    respondWithError(err, res);
  }
  
  function sortByRank(x, y) {
    if (x.rank === y.rank) return 0;
    if (x.rank < y.rank) return -1;
    return 1;
  }
  
  function addSimilarProducts(result) {
    if (result.products) {
      result.products.forEach(function (item) {
        data.matchingProducts.push(bbyProductToData(item));  
      });
      
      cb(data);
    }
    else {
      respondWithError({ stack: JSON.stringify(result) }, res);
    }
  }
  
  function handleSimilarProducts(result) {
    var skus = [];
    
    result.results.sort(sortByRank).forEach(function (item) {
      skus.push(item.sku);
    });
    
    if (skus.length > 0) {
      var search = "(sku in(" + skus.join(",") + ")&active=*)";
      bby.products(search, { show: "sku,name,image,modelNumber" })
        .then(addSimilarProducts)
        .catch(handleFailure);  
    }
    else {
      cb(data);
    }
  }
  
  function handleRequestedProduct(result) {
    if (result.products.length > 0) {
      data.requestedProduct = bbyProductToData(result.products[0]);
      bby.recommendations("similar", sku)
        .then(handleSimilarProducts)
        .catch(handleFailure);  
    }
    else {
      respondWithError({stack: "No products found."}, res);
    }
  }
  
  bby.products("(sku=" + sku + "&active=*)", { show: "sku,name,image,modelNumber" })
    .then(handleRequestedProduct)
    .catch(handleFailure);
}

app.get("/", getIndex)
app.get("/search-results", getSearchResults);

app.use(staticMiddleware("./out", { index: ["default.lnx", "index.html" ] }));

var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
  var host = server.address().address;
  console.log("The Like for Like application is listening at http://%s:%s", host, port);
});
