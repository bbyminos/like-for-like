var util = require("util");
var path = require("path");
var url = require("url");
var predicates = require("./predicates");
var urls = require("../../urls");
var nameMatches = predicates.nameMatches;
var isText = predicates.isText;
var isArray = predicates.isArray;
var isObject = predicates.isObject;
var hasProperty = predicates.hasProperty;

function hasHint(valueSpecTuple, hintName) {
  return valueSpecTuple.spec.hints.some(function (hint) {
    return hint === hintName;
  });
}

function hasPropertyWithHint(valueSpecTuple, hintName) {
  if (!valueSpecTuple.value) return false;
  if (!isObject(valueSpecTuple)) return false;
  
  for (var p in valueSpecTuple.value) {
    if (hasHint(valueSpecTuple.value[p], hintName)) return true; 
  }
  
  return false;
}

function hasBaseHint(valueSpecTuple) {
  return ["container", "link", "form", "submit", "content", "text"].some(function (baseHintName) {
    return hasHint(valueSpecTuple, baseHintName);
  });
}

exports.setSpecName = function setSpecName(valueSpecTuple) {
  if (!valueSpecTuple.name) return;
  valueSpecTuple.spec.name = valueSpecTuple.name;
};

exports.normalizeSpecHints = function normalizeSpecHints(valueSpecTuple) {
  var hints = valueSpecTuple.spec.hints || [];
  if (!util.isArray(hints)) hints = [hints];
  valueSpecTuple.spec.hints = hints;
};

exports.titleLabelsItsContainer = function titleLabelsItsContainer(valueSpecTuple) {
  if (hasProperty(valueSpecTuple, "title")) {
    valueSpecTuple.spec.labeledBy = "title";
  }
};

exports.inferTitle = function inferTitle(valueSpecTuple) {
  if (nameMatches(valueSpecTuple, /title/i) &&
    isText(valueSpecTuple) && !hasHint(valueSpecTuple, "label")) {
    valueSpecTuple.spec.hints.push("label");
  }
};

exports.inferLabel = function inferLabel(valueSpecTuple) {
  if (nameMatches(valueSpecTuple, /label$/i) && isText(valueSpecTuple)) {
    valueSpecTuple.spec.hints.push("label");
  }
};

exports.inferMarker = function inferMarker(realmBaseURI) {
  function tryInferMarker(valueSpecTuple) {
    if (hasProperty(valueSpecTuple, "for") && !hasHint(valueSpecTuple, "http://uncategorized/marker")) {
      valueSpecTuple.spec.hints.unshift("http://uncategorized/marker");
      convertToDataProperties(valueSpecTuple, ["for"]);
      valueSpecTuple.value.for = url.resolve(realmBaseURI, "." + valueSpecTuple.value.for);
    }
  }
  
  return function (valueSpecTuple) {
    if (nameMatches(valueSpecTuple, /marker$/i)) {
      tryInferMarker(valueSpecTuple);
    }
    else if (nameMatches(valueSpecTuple, /markers$/i) && util.isObject(valueSpecTuple.value)) {
      for (var p in valueSpecTuple.value) {
        tryInferMarker(valueSpecTuple.value[p]);
      }
    }
  };
};

function resolveVsps(valueSpecTuple) {
  if (!util.isObject(valueSpecTuple.value) || !("_templates" in valueSpecTuple.value)) return [ valueSpecTuple ];
  return valueSpecTuple.value._templates.map(function (template) {
    return template.content;
  });
}

exports.inferAttribute = function inferAttribute(valueSpecTuple) {
  function tryInferAttribute(valueSpecTuple) {
    var candidates = resolveVsps(valueSpecTuple);
    
    candidates.forEach(function (candidate) {
      if (!candidate) return;
      if (hasPropertyWithHint(candidate, "label") && !hasHint(candidate, "http://bestbuy.com/retail/attribute")) {
        candidate.spec.hints.unshift("http://bestbuy.com/retail/attribute");
      }
    });
  }
  
  if (nameMatches(valueSpecTuple, /attributes$/i) && util.isObject(valueSpecTuple.value)) {
    for (var p in valueSpecTuple.value) {
      tryInferAttribute(valueSpecTuple.value[p]);
    }
  }
};

exports.inferText = function inferText(valueSpecTuple) {
  if (isText(valueSpecTuple) && !hasHint(valueSpecTuple, "text")) {
    valueSpecTuple.spec.hints.push("text");
  }
};

exports.inferTextInput = function inferTextInput(valueSpecTuple) {
  if (hasHint(valueSpecTuple, "text") && valueSpecTuple.spec.input) {
    valueSpecTuple.spec.hints.splice(valueSpecTuple.spec.hints.length - 1, 0, "phrasing");
  }
};

exports.inferLink = function inferLink(valueSpecTuple) {
  if (hasProperty(valueSpecTuple, "href")) {
    if (!hasBaseHint(valueSpecTuple)) {
      valueSpecTuple.spec.hints.push("link");
    }
    convertToDataProperties(valueSpecTuple, ["href", "type"]);
    applyUrlsConvention(valueSpecTuple, "href");
  }
};

exports.inferImage = function inferImage(valueSpecTuple) {
  if (hasProperty(valueSpecTuple, "height") && hasProperty(valueSpecTuple, "width")) {
    if (!hasBaseHint(valueSpecTuple)) {
      valueSpecTuple.spec.hints.push("image");
    }
    convertToDataProperties(valueSpecTuple, ["height", "width"]);
  }
};

exports.inferContent = function inferContent(valueSpecTuple) {
  var hasSrc = hasProperty(valueSpecTuple, "src");
  var hasDataAndType = hasProperty(valueSpecTuple, "data") && hasProperty(valueSpecTuple, "type");

  if ((hasSrc || hasDataAndType)) {
    if (!hasBaseHint(valueSpecTuple)) {
      valueSpecTuple.spec.hints.push("content");
    }
    convertToDataProperties(valueSpecTuple, ["data", "type", "src", "scope", "alt"]);
    applyUrlsConvention(valueSpecTuple, "src");
  }
};

exports.inferForm = function inferForm(valueSpecTuple) {
  if (nameMatches(valueSpecTuple, /form$/i) && !hasBaseHint(valueSpecTuple)) {
    valueSpecTuple.spec.hints.push("form");
  }
};

exports.inferSubmit = function inferSubmit(valueSpecTuple) {
  if (hasProperty(valueSpecTuple, "action")) {
    if (!hasBaseHint(valueSpecTuple)) {
      valueSpecTuple.spec.hints.push("submit");
    }
    convertToDataProperties(valueSpecTuple, ["action", "method", "enctype"]);
    applyUrlsConvention(valueSpecTuple, "action");
  }
};

exports.inferInputSection = function inferInputSection(valueSpecTuple) {
  if (nameMatches(valueSpecTuple, /section$/i) && hasProperty(valueSpecTuple, "label") && !hasHint(valueSpecTuple, "section")) {
    valueSpecTuple.spec.hints.push("section");
    valueSpecTuple.spec.labeledBy = valueSpecTuple.spec.labeledBy || "label";

    function isInput(node) {
      return node.spec && !!node.spec.input;
    }

    var inputNode = find(valueSpecTuple, isInput);
    if (inputNode) {
      inputNode.spec.labeledBy = inputNode.spec.labeledBy || "label";
    }

    function isLabel(node) {
      return node.spec && node.spec.hints.indexOf("label") >= 0;
    }

    var labelNode = find(valueSpecTuple, isLabel);
    if (labelNode) {
      labelNode.spec.hints.unshift("header");
    }
  }
};

function find(node, sel) {
  if (sel(node)) return node;
  if (!util.isObject(node)) return null;

  for (var p in node) {
    if (sel(node[p])) return node[p];
    var result = find(node[p], sel);
    if (result) return result;
  }

  return null;
}

exports.inferSection = function inferSection(valueSpecTuple) {
  if ((hasProperty(valueSpecTuple, "header") || hasProperty(valueSpecTuple, "banner")) && !hasHint(valueSpecTuple, "section")) {
    valueSpecTuple.spec.hints.push("section");
    valueSpecTuple.spec.labeledBy = valueSpecTuple.spec.labeledBy || "header";
  }
};

exports.inferContainer = function inferContainer(valueSpecTuple) {
  if ((isArray(valueSpecTuple) || 
    isObject(valueSpecTuple)) && 
    !hasBaseHint(valueSpecTuple)) {
    valueSpecTuple.spec.hints.push("container");
  }
};

exports.inferHeader = function inferHeader(valueSpecTuple) {
  if (nameMatches(valueSpecTuple, /header|banner$/i)) {
    if (!hasHint(valueSpecTuple, "header")) valueSpecTuple.spec.hints.push("header");
    if (!hasHint(valueSpecTuple, "label")) valueSpecTuple.spec.hints.push("label");
  }
};

exports.removeRealmSpec = function removeRealmSpec(valueSpecTuple) {
  convertToDataProperties(valueSpecTuple, ["realm"]);
};

exports.removeScopeSpec = function removeScopeSpec(valueSpecTuple) {
  convertToDataProperties(valueSpecTuple, ["scope"]);
};


exports.addFollowDelay = function addFollowDelay(followDelay) {
  return function (valueSpecTuple) {
    if (valueSpecTuple.spec.follow !== 0) return;
    valueSpecTuple.spec.follow = followDelay;
  };
};

exports.inferMediaType = function inferMediaType(valueSpecTuple) {
  if (!valueSpecTuple.value) return;
  if (valueSpecTuple.value.type) return;
  if (hasHint(valueSpecTuple, "image")) return;
  if (!hasHint(valueSpecTuple, "link") && !hasHint(valueSpecTuple, "content")) return;
  
  valueSpecTuple.value.type = "application/lynx+json";
};

exports.applyRealmToDocument = function applyRealmToDocument(realmBaseURI) {
  return function (valueSpecTuple) {
    if (!valueSpecTuple.realm && valueSpecTuple.value && valueSpecTuple.value.realm) {
      valueSpecTuple.realm = valueSpecTuple.value.realm;
      delete valueSpecTuple.value.realm;
    }
    
    if (valueSpecTuple.realm) return;
    if (!valueSpecTuple.url) return;
    
    var realmPath = path.join(path.dirname(valueSpecTuple.url), "/");
    valueSpecTuple.realm = url.resolve(realmBaseURI, realmPath);
  };
};

function convertToDataProperties(valueSpecTuple, properties) {
  if (!util.isObject(valueSpecTuple.value)) return;

  properties.forEach(function (property) {
    if (property in valueSpecTuple.value &&
      (util.isObject(valueSpecTuple.value[property]) &&
      "value" in valueSpecTuple.value[property])) {
      valueSpecTuple.value[property] = valueSpecTuple.value[property].value;
    }
  });
}

function applyConventions(valueSpecTuple, options) {
  if (!("value" in valueSpecTuple)) return;
  conventions.forEach(function (convention) {
    convention(valueSpecTuple, options);
  });
}

function applyUrlsConvention(valueSpecTuple, key) {
  var hypertextRef = valueSpecTuple.value[key];
  if (!hypertextRef || hypertextRef.indexOf("urls.") !== 0) return;

  var urlKey = hypertextRef;
  valueSpecTuple.value[key] = "{{{" + urlKey + "}}}";
  urls.warnIfKeyNotFound(urlKey);
}
