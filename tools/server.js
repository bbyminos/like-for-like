var path = require("path");
var fs = require("fs");
var app = require("express")();
var tc = require("titlecase");
var urls = require("./build/urls");

// body parser middleware
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

var multer = require('multer');
app.use(multer()); // for parsing multipart/form-data

// static middleware
var staticMiddleware = require("serve-static");

staticMiddleware.mime.define({
  "application/lynx+json": ["lnx"],
  "application/lynx-spec+json;content=spec": ["lnxs"]
});

function applyCorsHeaders(response){
  response.setHeader("Access-Control-Allow-Origin", "*");
  response.setHeader("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,HEAD,OPTIONS");
}

// coerce GET method so POST/PUT/DELETE will function in static server. Remove parameters in segments
app.use(function (req, res, next) {
  req.method = "GET";
  req.url = urls.removeSegmentParameters(req.url);
  applyCorsHeaders(res);
  next();
});

app.get(/states.lnx$/, function(req, res, next){
  var baseDir = path.parse(req.url).dir;
  var dir = path.join("./out", baseDir);

  var lnxFileEx = /.lnx$/;
  function createStatesDocument(err, files) {
    if(!files || err) return next();
    var lnxFiles = files.filter(function(file){
      return lnxFileEx.test(file);
    });
    
    var links = lnxFiles.map(function(file){
      return { 
        title: tc.toTitleCase(file.replace(/-/g, " ").replace(".lnx", "")),
        href: path.join(baseDir, file).replace(/\\/g, "/")
      };
    });
    
    if(links.length === 0) return next();
    
    var states = {
      title: baseDir,
      states: links,
      spec: {
        hints: [ "http://lynx-json.org/design/states", "object" ],
        children: [
          {
            name: "title",
            hints: [ "title", "text" ]
          },
          {
            name: "states",
            hints: [ "array" ],
            children: {
              hints: [ "link" ],
              children: [
                {
                  name: "title",
                  hints: [ "title", "text" ]
                }
              ]
            }
          }
        ]
      }
    };
    
    res.set("Content-Type", "application/lynx+json");
    res.send(states);
    res.end();
  }

  fs.readdir(dir, createStatesDocument);
});

app.use(staticMiddleware("./out", { index: ["default.lnx", "index.html" ] }));

// index middleware
var indexMiddleware = require("serve-index");
app.use(indexMiddleware("./out", {'icons': true}));

var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
  var host = server.address().address;
  console.log("Lynx static server is listening at http://%s:%s", host, port);
});
