global._ = require('lodash');
global.Promise = require('bluebird');

// check the command line options for a -env=someEnvironment
var environment = _(process.argv)
    .map(item => {

        // unfortunately mocha mangles the env=something, so using regex to check both
        // ex:
        // -env-integration-testing
        // -env=integration-testing
        var match = /-env(?:-|=)((?:\w|-)+)/gi.exec(item);
        if (match && match[1]) {
            return match[1];
        }
    })
    .compact()
    .first();

environment = environment || process.env.NODE_ENV || 'development';

var config = require('./env/' + environment);
config.consoleLogLevel = {
    debug: "debug",
    info: "info",
    error: "error"
}

module.exports = config;