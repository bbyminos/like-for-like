var config = module.exports;

config.environment = 'production';

config.express = {
    hostName: 'pdl01dpaap002',
    port: 3000,
    ip: '127.0.0.1'
};

config.logToElasticsearchUrl = 'http://pll02dpaap002.na.bestbuy.com:9200';
config.productMatchingServiceUrl = (process.env.likeForLikeDataServiceUri || 'http://pll02o9eap28d.na.bestbuy.com/') + 'like-for-like/';
//config.availabilityServiceUrl = 'http://ptl01omsap02a:30280/yantra/webresources/fulfillmentOptions/api/v2/';
config.availabilityServiceUrl = 'https://omssrvc1eoms.bestbuy.com/yantra/webresources/fulfillmentOptions/api/v1/itemInquiry/';

config.bbydeveloperKey = 'wvsmnrnnfus23u99thfexpy6';
config.applicationId = 'CoreBlue';

config.db = {
    host: 'pll02dpaap001.na.bestbuy.com',
    name: 'lynx-like-for-like',
    port: '27017'
};
