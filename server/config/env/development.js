var config = module.exports;

config.environment = 'development';

config.express = {
    hostName: 'localhost',
    port: 3001,
    ip: '127.0.0.1'
};

config.productMatchingServiceUrl = (process.env.likeForLikeDataServiceUri || 'http://pll02dpawb004.na.bestbuy.com:8080/') + 'like-for-like/';
//config.productMatchingServiceUrl = 'http://pll02o9eap28d.na.bestbuy.com/like-for-like/';
config.availabilityServiceUrl = 'http://ptl01omsap02a.na.bestbuy.com:30280/yantra/webresources/fulfillmentOptions/api/v1/itemInquiry/';

config.bbydeveloperKey = 'wvsmnrnnfus23u99thfexpy6';
config.applicationId = 'CoreBlue';

config.db = {
    host: 'localhost',
    name: 'lynx-like-for-like',
    port: '27017'
};