var winston = require('winston');
var Elasticsearch = require('winston-elasticsearch');
var config = require('./config');
var moment = require('moment');
var os = require('os');

function now() {
    return moment().format();
}

Elasticsearch.prototype.name = 'elasticsearch'; //name so we can refer to it later

module.exports = global.log = new winston.Logger({
    transports: [
        new winston.transports.Console({
            level: config.consoleLogLevel.info,
            colorize: true,
            timestamp: now,
            prettyPrint: true
        }),
        new Elasticsearch({
            level: 'info',
            source: os.hostname(),
            fireAndForget: true,
            index: 'like-for-like-' + (config.environment || 'develop'),
            clientOpts: {
                host: config.logToElasticsearchUrl || 'localhost:9200',
                log: [{
                    type: 'stdio',
                    levels: []
                }]
            },
            disable_fields: true
        })
    ]
})