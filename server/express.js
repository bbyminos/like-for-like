var express = require('express');
var app = express();
var exphbs = require('express-handlebars');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');

// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// static stuff
app.use('/node_modules', express.static('node_modules'));
app.use('/client/fonts', express.static('src/fonts'));
express.static.mime.default_type = "application/lynx-spec+json";
app.use(express.static('out', { maxAge: 86400000 })); //86400000 is oneDay
app.use(favicon('server/favicon.ico'));

// configure templates
app.engine('.mustache', exphbs({defaultLayout: false, extname: '.mustache', layoutsDir: process.cwd() + '/out' }));
app.set('view engine', '.mustache');
app.set('views', process.cwd() + '/out');

app.use(function (req, res, next) {
    if (req.originalUrl === '/') {
        res.redirect(301, '/client?url=/default');
    }
    next()
});

// set content type on responses to lynx+json
app.use(function (req, res, next) {
    if (req.originalUrl != '/client?url=/default') {
        res.set("Cache-Control", "no-cache,max-age=0")
        res.set("Content-Type", "application/lynx+json");
    }
    next();
});

// route config
require('./routes')(app);

module.exports = app;