var expect = require('expect');
var Promise = global.Promise = require('bluebird');
Promise.promisifyAll(require('request'));
var storeService = require('../../server/services/storeService');

var knownZipCode = 55423;

describe('storeService', function() {
    describe('getClosestStores', function () {
        it('promise should return a type of array', function (done) {
            storeService.closestStoresByZipcode(knownZipCode)
                .then(function(result) {
                    //console.log(result);
                    expect(result).toBeA('array');
                    done();
                })
        });

        it('promise should return more than 2 stores for knownZipCode', function (done) {
            storeService.closestStoresByZipcode(knownZipCode)
                .then(function(result) {
                    //console.log(result);
                    expect(result.length).toBeGreaterThan(0);
                    done();
                })
        });
    });
});