// GET https://api.bestbuy.com/v1/stores(area(55423,25)&storeType=BigBox)?format=json&show=storeId,storeType,city,region&pageSize=50&apiKey=wvsmnrnnfus23u99thfexpy6
// Currently this only looks at BigBox stores because of performance issues.
var storeService = module.exports;
var request = require('request');
var config = require('../config/config');
var _ = require('lodash');

var defaultRadius = 25;

storeService.closestStoresByZipcode = function(zipCode) {
    var options = {
        url: `https://api.bestbuy.com/v1/stores(area(${zipCode},${defaultRadius})&storeType=BigBox)?format=json&show=storeId,storeType,city,region,name&pageSize=1&apiKey=wvsmnrnnfus23u99thfexpy6`,
        strictSSL: false
    };

    if (config.environment != 'development') {
        options.proxy = 'http://delicious-pox-secondarymarkets.cdc.bestbuy.com:3128';
        log.info(`using proxy: ${options.proxy}`);
    }

    return request.getAsync(options).then(response => JSON.parse(response.body).stores);
};

function parseSafe(str) {
    return _.attempt(JSON.parse.bind(null, str));
}

//"stores": [
//    {
//        "storeId": 281,
//        "storeType": "BigBox",
//        "city": "Richfield",
//        "region": "MN"
//    },
//    {
//        "storeId": 1000,
//        "storeType": "BigBox",
//        "city": "Bloomington",
//        "region": "MN"
//    },