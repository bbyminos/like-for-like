var matchingService = require('../../server/services/matchingService');
var config = require('../config/config');
var expect = require('expect');
var request = require('request');
var Promise = global.Promise = require('bluebird');
Promise.promisifyAll(require('request'));
var _ = require('lodash');

var knownSku = 1725534;

describe('matchingService >', function() {

    describe('matchBasic >', function () {

        it('should return a result with sku details', function (done) {
            matchingService.matchBasicNoCache(knownSku)
                .then(function(result) {
                    expect(result).toExist();
                    expect(result.sku).toExist();
                    done();
                })
        });

        it('similarSkus should be an array', function (done) {
            matchingService.matchBasicNoCache(knownSku)
                .then(function(result) {
                    expect(result).toExist();
                    expect(result.similarSkus).toExist();
                    expect(result.similarSkus).toBeA('array');
                    expect(result.similarSkus.length).toBeGreaterThan(0);
                    done();
                })
        });
    });

    describe('matchFiltered >', function() {

        it('should filter based on brand (Samsung) - no bluebird', function (done) {

            //var facet = { "brand": ["Samsung"] };
            var facet = { brand: ['Samsung'] };

            var data = [{
                "displayName": "Brand",
                "name": "brand",
                "or": facet.brand
            }];

            var options = {
                url: config.productMatchingServiceUrl + 1725534 + '/filtered',
                json: true,
                body: data
            };

            request.post(options, function (err, res, body) {
                expect(body).toExist();
                expect(body.sku).toExist();
                expect(body.similarSkus).toExist();
                expect(body.similarSkus[0].brand).toBe('Samsung');
                done();
            });
        });

        it('filter based on brand (Sony) - bluebird', function (done) {

            var facet = { brand: ['Sony'] };

            matchingService.matchFiltered(knownSku, facet)
                .then(function(result) {
                    expect(result).toExist();
                    expect(result.sku).toExist();
                    expect(result.similarSkus).toExist();
                    expect(result.similarSkus[0].brand).toBe('Sony');
                    done();
                })
        });

        it('filter based on refresh rate (240Hz) - bluebird', function (done) {

            //https://trello.com/c/IC6zYNO4

            var facet = { refresh_rate: ['240Hz'] };

            matchingService.matchFiltered(2135063, facet)
                .then(function(result) {
                    expect(result).toExist();
                    expect(result.sku).toExist();
                    expect(result.similarSkus).toExist();
                    expect(result.similarSkus.length).toBe(1);
                    done();
                })
        });

        it('split out spec names for facet', function (done) {
            var refresh_rate = 'specifications.televisions:refresh_rate';
            var vertical_resolution = 'specifications.televisions:vertical_resolution';

            expect(refresh_rate.split(':')[1]).toBe('refresh_rate');
            expect(vertical_resolution.split(':')[1]).toBe('vertical_resolution');
            done();
        });

        it('should find a specficiation to split', function (done) {
            var str = 'specifications.televisions:vertical_resolution';
            expect(str.indexOf(':')).toBeGreaterThan(0);
            done();
        });

        it('create filter with multiple values', function (done) {

            var query = {
                brand: ['VIZIO', 'Samsung'],
                refresh_rate: '120Hz',
                vertical_resolution: '2160p (4K)'
            };

            var faceted = matchingService.addFilter(query);
            expect(faceted).toExist();
            //brand is multiple values
            expect(faceted[0].or[0]).toBe('VIZIO');
            expect(faceted[0].or[1]).toBe('Samsung');
            //others are single valued
            expect(faceted[1].name).toBe('specifications.televisions:refresh_rate');
            expect(faceted[1].or).toBe('120Hz');
            expect(faceted[2].name).toBe('specifications.televisions:vertical_resolution');
            expect(faceted[2].or).toBe('2160p (4K)');
            done();
        });

        it('should filter based on 3D Technology', function (done) {

            //https://trello.com/c/ki6l0M77/115-sku-4846856-is-not-filtering-by-3d-properly

            var facet = { three_d_technology: ['true'] };

            matchingService.matchFiltered(4846856, facet)
                .then(function(result) {
                    expect(result).toExist();
                    var attr = _.find(result.similarSkus[0].specifications, { displayName: '3D Technology' });
                    log.info(attr);
                    expect(attr.values[0]).toBe('true');
                    done();
                })
        });
    });
});

//vertical_resolution: '2160p (4K)' }
//refresh_rate: '120Hz',
