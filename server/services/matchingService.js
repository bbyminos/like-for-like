// http://pll02dpawb004.na.bestbuy.com:8080/like-for-like/1511709
var matchingService = module.exports;
var request = require('request');
var config = require('../config/config');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var matchingResponse = require('./matchingResponse');
var _ = require('lodash');

matchingService.match = function(sku, query)
{
    if (query && (query.brand || query.refresh_rate || query.vertical_resolution || query.three_d_technology)) {
        return matchingService.matchFiltered(sku, query);
    }

    return matchingService.matchBasic(sku);
};

matchingService.matchBasic = function(sku) {

    return matchingResponse.findOne({skuId: sku})
        .then(data => {

            if (data && data.body) {
                log.info(`cache: ${sku}`);
                return parseSafe(data.body);
            }

            log.info(`no cache: ${sku}`);
            return request.getAsync(config.productMatchingServiceUrl + sku)
                .then(response => {

                    // if the response is valid then cache it
                    if (response.statusCode === 200) {
                        var matchingData = new matchingResponse({
                            skuId: sku,
                            body: response.body
                        });
                        matchingData.save();
                    }

                    return parseSafe(response.body);
                });
        });
};

//mostly using this for testing
matchingService.matchBasicNoCache = function (sku) {
    return request.getAsync(config.productMatchingServiceUrl + sku)
        .then(response =>  parseSafe(response.body));
};

matchingService.matchFiltered = function(sku, facets)
{
    var options = {
        url: config.productMatchingServiceUrl + sku + '/filtered',
        json: true,
        body: matchingService.addFilter(facets)
    };

    log.info(options.body);

    return request.postAsync(options).then(response => response.body); // don't re-parse body
};

matchingService.addFilter = function(facets) {

    var obj = [];

    if (facets.brand) {
        obj.push({
            "displayName": "Brand",
            "name": "brand",
            "or": facets.brand
        })
    }

    if (facets.refresh_rate) {
        obj.push({
            "displayName": "Refresh Rate",
            "name": "specifications.televisions:refresh_rate",
            "or": facets.refresh_rate
        })
    }

    if (facets.vertical_resolution) {
        obj.push({
            "displayName": "Vertical Resolution",
            "name": "specifications.televisions:vertical_resolution",
            "or": facets.vertical_resolution
        })
    }

    if (facets.three_d_technology) {
        obj.push({
            "displayName": "3D Technology",
            "name": "specifications.televisions:three_d_technology",
            "or": facets.three_d_technology
        })
    }

    return obj;
};

function parseSafe(str) {
    return _.attempt(JSON.parse.bind(null, str));
}