var mongoose = require('mongoose');

var matchingResponseSchema = new mongoose.Schema({
    skuId: {
        type: String
    },
    body: {
        type: String
    }
});

module.exports = mongoose.model('matchingResponse', matchingResponseSchema);