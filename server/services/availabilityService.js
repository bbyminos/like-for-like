// Testing Oms.
//  GET http://ptl01omsap02a:30280/yantra/webresources/fulfillmentOptions/api/v1/itemInquiry/5458583/spu?zipCode=55423
//  Remember to set your headers, Application-ID: CoreBlue and SellingStoreId: BBY_281
//  For testing, set the sku to 5458583 to ensure a result from OMS for zip code 55423, store BBY_281
var availabilityService = module.exports;
var config = require('../config/config');
var request = require('request');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');

var appId = 'CoreBlue';
var certFile = path.resolve(__dirname, 'ssl/cert.pem');
var keyFile = path.resolve(__dirname, 'ssl/key.pem');

var options = {
    strictSSL: false,
    headers: {
        'Application-ID': appId // need to get our own id
    },
    cert: fs.readFileSync(certFile),
    key: fs.readFileSync(keyFile),
    passphrase: 'dude'
};

availabilityService.fulfillmentOptionsBySku = function(sku, bbystore) {
    options.url = `${config.availabilityServiceUrl}${sku}`;
    options.headers.SellingStoreId = `BBY_${bbystore.storeId}`;

    return request.getAsync(options).then(response => parseSafe(response.body));
};

availabilityService.getDeliveryAvailability = function(sku, zipCode, bbystore) {
    options.url = `${config.availabilityServiceUrl}${sku}/del/?zipCode=${zipCode}`;
    options.headers.SellingStoreId = `BBY_${bbystore.storeId}`;

    return request.getAsync(options).then(response => parseSafe(response.body));
};

availabilityService.getStorePickupAvailability = function(sku, zipCode, bbystore) {
    options.url = `${config.availabilityServiceUrl}${sku}/spu/?zipCode=${zipCode}`;
    options.headers.SellingStoreId = `BBY_${bbystore.storeId}`;

    return request.getAsync(options).then(response => parseSafe(response.body));
};

function parseSafe(str) {
    return _.attempt(JSON.parse.bind(null, str));
}