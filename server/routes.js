var express = require('express');
var homeController = require('./controllers/homeController');
var searchResultsController = require('./controllers/searchResultsController');
var facetController = require('./controllers/facetController');
var comparisonController = require('./controllers/comparisonController');

module.exports = function (app) {
    app.get('/default', homeController.index);
    app.get('/search-results', searchResultsController.index);
    app.get('/search-results/availability', searchResultsController.checkAvailability);
    app.get('/search-results/availability/status/:id', searchResultsController.checkAvailabilityStatus);
    app.get('/search-results/refine/:sku', facetController.getFacets);
    app.get('/comparison', comparisonController.index);
};