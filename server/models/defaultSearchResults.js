var urls = require('./defaultUrls');

module.exports.model = function model() {
    return {
        originalProduct: {
            image: {
                url: "http://placehold.it/100x100",
                width: 111,
                height: 73
            },
            shortDescription: "Product Short Description",
            longDescription: "Long Description"
        },
        matchingProducts: [],

        "screenSize": {
            "textState": "unknown",
            "requiredState": "unknown",
            "value": ""
        },
        "urls": urls
    }
};



//"attributes": [
//    {
//        "text": "458960",
//        "label": "SKU"
//    },
//    {
//        "text": "VIZIO",
//        "label": "Brand"
//    },
//    {
//        "text": "M552I-B2",
//        "label": "Model Number"
//    },
//    {
//        "text": "Black",
//        "label": "Color"
//    },
//    {
//        "text": "LED",
//        "label": "Display Type"
//    },
//    {
//        "text": "240Hz",
//        "label": "Refresh Rate"
//    },
//    {
//        "text": 4,
//        "label": "Number Of HDMI Inputs"
//    },
//    {
//        "text": 1,
//        "label": "Number Of USB Port(s)"
//    },
//    {
//        "text": "55 inches",
//        "label": "Screen Size Class"
//    },
//    {
//        "text": "1080p",
//        "label": "Vertical Resolution"
//    },
//    {
//        "text": false,
//        "label": "PC Input"
//    },
//    {
//        "text": 0,
//        "label": "Number Of DVI Inputs"
//    }
//],


//"matchingProducts": [
//    {
//        "attributes": [
//            {
//                "text": "458960",
//                "label": "SKU"
//            },
//            {
//                "text": "VIZIO",
//                "label": "Brand"
//            },
//            {
//                "text": "M552I-B2",
//                "label": "Model Number"
//            },
//            {
//                "text": "Black",
//                "label": "Color"
//            },
//            {
//                "text": "LED",
//                "label": "Display Type"
//            },
//            {
//                "text": "240Hz",
//                "label": "Refresh Rate"
//            },
//            {
//                "text": 4,
//                "label": "Number Of HDMI Inputs"
//            },
//            {
//                "text": 1,
//                "label": "Number Of USB Port(s)"
//            },
//            {
//                "text": "55 inches",
//                "label": "Screen Size Class"
//            },
//            {
//                "text": "1080p",
//                "label": "Vertical Resolution"
//            },
//            {
//                "text": false,
//                "label": "PC Input"
//            },
//            {
//                "text": 0,
//                "label": "Number Of DVI Inputs"
//            }
//        ],
//        "image": {
//            "url": "http://placehold.it/100x100",
//            "width": 111,
//            "height": 73
//        },
//        "shortDescription": "Product Short Description",
//        "longDescription": "Long Description"
//    },
//    {
//        "attributes": [
//            {
//                "text": "458960",
//                "label": "SKU"
//            },
//            {
//                "text": "VIZIO",
//                "label": "Brand"
//            },
//            {
//                "text": "M552I-B2",
//                "label": "Model Number"
//            },
//            {
//                "text": "Black",
//                "label": "Color"
//            },
//            {
//                "text": "LED",
//                "label": "Display Type"
//            },
//            {
//                "text": "240Hz",
//                "label": "Refresh Rate"
//            },
//            {
//                "text": 4,
//                "label": "Number Of HDMI Inputs"
//            },
//            {
//                "text": 1,
//                "label": "Number Of USB Port(s)"
//            },
//            {
//                "text": "55 inches",
//                "label": "Screen Size Class"
//            },
//            {
//                "text": "1080p",
//                "label": "Vertical Resolution"
//            },
//            {
//                "text": false,
//                "label": "PC Input"
//            },
//            {
//                "text": 0,
//                "label": "Number Of DVI Inputs"
//            }
//        ],
//        "image": {
//            "url": "http://placehold.it/100x100",
//            "width": 111,
//            "height": 73
//        },
//        "shortDescription": "Product Short Description",
//        "longDescription": "Long Description"
//    },
//    {
//        "attributes": [
//            {
//                "text": "458960",
//                "label": "SKU"
//            },
//            {
//                "text": "VIZIO",
//                "label": "Brand"
//            },
//            {
//                "text": "M552I-B2",
//                "label": "Model Number"
//            },
//            {
//                "text": "Black",
//                "label": "Color"
//            },
//            {
//                "text": "LED",
//                "label": "Display Type"
//            },
//            {
//                "text": "240Hz",
//                "label": "Refresh Rate"
//            },
//            {
//                "text": 4,
//                "label": "Number Of HDMI Inputs"
//            },
//            {
//                "text": 1,
//                "label": "Number Of USB Port(s)"
//            },
//            {
//                "text": "55 inches",
//                "label": "Screen Size Class"
//            },
//            {
//                "text": "1080p",
//                "label": "Vertical Resolution"
//            },
//            {
//                "text": false,
//                "label": "PC Input"
//            },
//            {
//                "text": 0,
//                "label": "Number Of DVI Inputs"
//            }
//        ],
//        "image": {
//            "url": "http://placehold.it/100x100",
//            "width": 111,
//            "height": 73
//        },
//        "shortDescription": "Product Short Description",
//        "longDescription": "Long Description"
//    },
//    {
//        "attributes": [
//            {
//                "text": "458960",
//                "label": "SKU"
//            },
//            {
//                "text": "VIZIO",
//                "label": "Brand"
//            },
//            {
//                "text": "M552I-B2",
//                "label": "Model Number"
//            },
//            {
//                "text": "Black",
//                "label": "Color"
//            },
//            {
//                "text": "LED",
//                "label": "Display Type"
//            },
//            {
//                "text": "240Hz",
//                "label": "Refresh Rate"
//            },
//            {
//                "text": 4,
//                "label": "Number Of HDMI Inputs"
//            },
//            {
//                "text": 1,
//                "label": "Number Of USB Port(s)"
//            },
//            {
//                "text": "55 inches",
//                "label": "Screen Size Class"
//            },
//            {
//                "text": "1080p",
//                "label": "Vertical Resolution"
//            },
//            {
//                "text": false,
//                "label": "PC Input"
//            },
//            {
//                "text": 0,
//                "label": "Number Of DVI Inputs"
//            }
//        ],
//        "image": {
//            "url": "http://placehold.it/100x100",
//            "width": 111,
//            "height": 73
//        },
//        "shortDescription": "Product Short Description",
//        "longDescription": "Long Description"
//    },
//    {
//        "attributes": [
//            {
//                "text": "458960",
//                "label": "SKU"
//            },
//            {
//                "text": "VIZIO",
//                "label": "Brand"
//            },
//            {
//                "text": "M552I-B2",
//                "label": "Model Number"
//            },
//            {
//                "text": "Black",
//                "label": "Color"
//            },
//            {
//                "text": "LED",
//                "label": "Display Type"
//            },
//            {
//                "text": "240Hz",
//                "label": "Refresh Rate"
//            },
//            {
//                "text": 4,
//                "label": "Number Of HDMI Inputs"
//            },
//            {
//                "text": 1,
//                "label": "Number Of USB Port(s)"
//            },
//            {
//                "text": "55 inches",
//                "label": "Screen Size Class"
//            },
//            {
//                "text": "1080p",
//                "label": "Vertical Resolution"
//            },
//            {
//                "text": false,
//                "label": "PC Input"
//            },
//            {
//                "text": 0,
//                "label": "Number Of DVI Inputs"
//            }
//        ],
//        "image": {
//            "url": "http://placehold.it/100x100",
//            "width": 111,
//            "height": 73
//        },
//        "shortDescription": "Product Short Description",
//        "longDescription": "Long Description"
//    }
//],