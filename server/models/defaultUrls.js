module.exports  = {
    images: {
        search: "/images/search.svg",
        back: "/images/back.svg"
    },
    index: "/default",
    searchResults: {
        index: "/search-results",
        availability: {
            index: "/availability"
        },
        refine: {
            index: '/search-results'
        }
    },
    comparison: "/comparison"
}