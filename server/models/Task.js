var mongoose = require('mongoose');

var taskSchema = new mongoose.Schema({
    taskId: {
        type: String
    },
    message: {
        type: String
    },
    result: {
        pending: {
            type: Boolean
        },
        availabilityIconSrc: {
            type: String
        },
        availabilityMessage: {
            type: String
        }
    }
});

module.exports = mongoose.model('Task', taskSchema);