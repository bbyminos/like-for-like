var urls = require('./defaultUrls');

module.exports.model = function model() {
    return {
        sku: {
            value: undefined,
            textState: "valid",
            requiredState: "valid"
        },
        originalPurchasePrice: {
            "value": ""
        },
        zipCode: {
            value: undefined,
            textState: "valid",
            requiredState: "valid"
        },
        urls: urls
    }
};