var urls = require('./defaultUrls');

module.exports.model = function model() {
    return {
        results: [
            {
                "header": null,
                "isHeader": true,
                "original": {
                    "currentMarketValue": "",
                    "image": {
                        "url": "http://placehold.it/100x100",
                        "width": 111,
                        "height": 73
                    },
                    "title": "Original",
                    "shortDescription": "Product Short Description",
                    "longDescription": "Product Long Description"
                },
                "comparison": {
                    "shortDescription": "Product Short Description",
                    "image": {
                        "url": "http://placehold.it/100x100",
                        "width": 111,
                        "height": 73
                    },
                    "title": "Comparison",
                    "averagePrice": "",
                    "longDescription": "Product Long Description"
                }
            }
        ],
        urls: urls
    }
};



//},
//{
//    "header": "Resolution",
//    "original": "1080i",
//    "comparison": "1080p"
//},
//{
//    "header": "Size",
//    "original": "40\\\"",
//    "comparison": "43\\\""
//},
//{
//    "header": "Attribute 3",
//    "original": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
//    "comparison": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
//},
//{
//    "header": "Attribute 4",
//    "original": "Orig 4",
//    "comparison": "Compare 4"
//},
//{
//    "header": "Resolution",
//    "original": "1080i",
//    "comparison": "1080p"
//},
//{
//    "header": "Size",
//    "original": "40\\\"",
//    "comparison": "43\\\""
//},
//{
//    "header": "Attribute 3",
//    "original": "Orig 3",
//    "comparison": "Compare 3"
//},
//{
//    "header": "Attribute 4",
//    "original": "Orig 4",
//    "comparison": "Compare 4"
//},
//{
//    "header": "Attribute 5",
//    "original": "Orig 5",
//    "comparison": "Compare 5"
//},
//{
//    "header": "Resolution",
//    "original": "1080i",
//    "comparison": "1080p"
//},
//{
//    "header": "Size",
//    "original": "40\\\"",
//    "comparison": "43\\\""
//},
//{
//    "header": "Attribute 3",
//    "original": "Orig 3",
//    "comparison": "Compare 3"
//},
//{
//    "header": "Attribute 4",
//    "original": "Orig 4",
//    "comparison": "Compare 4"
//},
//{
//    "header": "Attribute 5",
//    "original": "Orig 5",
//    "comparison": "Compare 5"
//}