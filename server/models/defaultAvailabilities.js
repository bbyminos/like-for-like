module.exports = {

    default: {
        pending: true
    },

    del_available: {
        pending: false,
        availabilityIconSrc: "/images/del-available.svg",
        availabilityMessage: "Available"
    },

    del_unavailable: {
        pending: false,
        availabilityIconSrc: "/images/del-unavailable.svg",
        availabilityMessage: "Not Available"
    },

    spu_available: {
        pending: false,
        availabilityIconSrc: "/images/spu-available.svg",
        availabilityMessage: "Available"
    },

    spu_unavailable: {
        pending: false,
        availabilityIconSrc: "/images/spu-unavailable.svg",
        availabilityMessage: "Not Available"
    }
}