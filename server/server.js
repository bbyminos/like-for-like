global.log = require('./config/log');
var Promise = global.Promise = require('bluebird');

var app = Promise.promisifyAll(require('./express'));
Promise.promisifyAll(require('request'));
var config = require('./config/config');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

module.exports.run = function (cb) {

    log.info('server - Starting "' + config.environment + '"');

    return connectToMongoose()
        .then(() => {
            app.listenAsync(config.express.port)
        })
        .then(() => log.info(`running on port ${config.express.port}`))
        .catch((error) => log.error('server - Error while starting', error));
};

function connectToMongoose() {
    var options = {
        server: {
            auto_reconnect: true,
            socketOptions: {
                keepAlive: 300000,
                connectTimeoutMS: 30000
            }
        },
        replset: {
            socketOptions: {
                keepAlive: 300000,
                connectTimeoutMS: 30000 }
        }
    };

    var url = `mongodb://${config.db.host}:${config.db.port}/${config.db.name}`;

    var db = mongoose.connection;

    db.on('reconnected', function () {
        log.warn('MongoDB reconnected!');
    });
    db.on('disconnected', function () {
        log.warn('MongoDB disconnected!');
    });

    return mongoose.connect(url, options)
        .then(() => {
            log.info(`connected to ${url}`);
        })
}

