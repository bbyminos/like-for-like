var _ = require('lodash');
var attributeHelper = require('./attributeHelper');

// default value
var defaulted = "na";

// Build the product model and put all modifying logic here
// where dest is a product from the es query
module.exports.map = function (src, dest) {

    dest.currentPrice = _.get(src, 'prices.current', defaulted);
    dest.averagePrice = _.get(src, 'prices.current', defaulted);
    dest.marketValue = _.get(src, 'cost', defaulted);
    dest.averageCost = _.get(src, 'cost', defaulted);

    if (dest.marketValue != defaulted) {
        dest.marketValue = formatCurrency(dest.marketValue);
    }

    if (dest.averageCost != defaulted) {
        dest.averageCost = formatCurrency(dest.averageCost);
    }

    if (dest.currentPrice != defaulted) {
        dest.currentPrice = formatCurrency(dest.currentPrice);
    }

    if (dest.averagePrice != defaulted) {
        dest.averagePrice = formatCurrency(dest.averagePrice);
    }


    dest.shortDescription = parseSafe(_.get(src, 'skuDescription', defaulted).replace(/['"]+/g, ''));
    dest.longDescription = parseSafe(_.get(src, 'descriptions.long', defaulted));
    dest.availabilityResult = _.get(src, 'availabilityResult', 0);

    // image
    _.set(dest, 'image.url', _.get(src, 'images[0].href', defaulted));
    _.set(dest, 'image.width', 111);
    _.set(dest, 'image.height', 73);

    dest.attributes = [];
    dest.comparableAttributes = [];
    addAttribute(dest, _.get(src, 'skuId', defaulted), "SKU");
    addComparableAttribute(dest, _.get(src, 'skuId', defaulted), "SKU");

    addAttribute(dest, _.get(src, 'brand', defaulted), "Brand");
    addComparableAttribute(dest, _.get(src, 'brand', defaulted), "Brand");

    addAttribute(dest, _.get(src, 'manufacturer.modelNumber', defaulted), "Model Number");
    addComparableAttribute(dest, _.get(src, 'manufacturer.modelNumber', defaulted), "Model Number");

    addAttribute(dest, _.get(src, 'color.displayName', defaulted), "Color");
    addComparableAttribute(dest, _.get(src, 'color.displayName', defaulted), "Color");

    // load up all those magellan specficiations for the comparison view
    src.specifications.forEach(spec => addComparableAttribute(dest, _.get(spec, 'values[0]', defaulted), _.get(spec, 'displayName', defaulted)));

    // change what displayable attributes are displayed based in search-results the product type
    var extendedAttributes = [];
    if (!src.class) {
        return dest;
    }

    switch(src.class.id) {
        case "140":
            extendedAttributes = attributeHelper.laptopAttributes;
            break;
        case "276":
            extendedAttributes = attributeHelper.televisionAttributes;
            break;
        case "306":
            extendedAttributes = attributeHelper.televisionAttributes;
            break;
        default:
            extendedAttributes = attributeHelper.defaultAttributes;
    }

    extendedAttributes.forEach(displayName => {
        var attr = _.find(src.specifications, { 'displayName': displayName });
        addAttribute(dest, _.get(attr, 'values[0]', defaulted), _.get(attr, 'displayName', defaulted));
    });

    dest.attributeGroups = [3];
    dest.attributeGroups[0] = { "attributes": _.slice(dest.attributes, 0, 3) };
    dest.attributeGroups[1] = { "attributes": _.slice(dest.attributes, 4, 7) };
    dest.attributeGroups[2] = { "attributes": _.slice(dest.attributes, 8, dest.attributes.length) };

    return dest;
};

function addAttribute(product, text, label) {
    if (!product.attributes) return;

    if (text === defaulted) return;

    product.attributes.push({
        text: text.replace(/['"]+/g, '').trim(),  // magellan puts html all over the place...
        label: label
    });
}

function addComparableAttribute(product, text, label) {
    if (!product.comparableAttributes) return;

    if (text === defaulted) return;

    product.comparableAttributes.push({
        text: parseSafe(text),  // magellan puts html all over the place...
        label: label
    });
}

function parseSafe(str) {
    return str.replace(/<(?:.|\n)*?>/gm, '').replace(/['"]+/g, '').replace(/\r?\n|\r/g, '');
}

function formatCurrency(value) {

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2
    });

    return formatter.format(value);
}