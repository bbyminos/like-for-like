var facetController = module.exports;
var matchingService = require('../services/matchingService');

facetController.getFacets = function (req, res, next) {

    matchingService.match(req.params.sku, req.query)
        .then(result => {
            // create default object
            var dataModel = {
                searchGroups: [
                ]
            };

            // if nothing do nothing
            if (!result.facets) return dataModel;

            // for each search facet add a search group
            result.facets.forEach(facet => addSearchGroup(dataModel.searchGroups, facet));

            //need to move facets around for ease of use
            var needsToMoveFacet = dataModel.searchGroups.shift();
            dataModel.searchGroups.push(needsToMoveFacet);

            // static doc uses searchResults index url as action destination, this is a form though
            // so querystring parameters will be replaced by inputs, i.e { Brand: 'LG' }
            // need to revisit this as I'm still not sure how this works via lynx
            dataModel.urls = {
                searchResults: {
                    index: `/search-results`
                }
            };

            // hidden inputs will be automatically added as querystring
            dataModel.inputs = [
                { name: 'sku', value: req.params.sku },
                { name: 'zipCode', value: req.query.zipCode },
                { name: 'originalPurchasePrice', value: req.query.originalPurchasePrice }
            ];

            return dataModel;
        })
        .then(model => {
            res.render('./search-results/refine/default', model);
        });
};

function addSearchGroup(searchGroups, searchFacet) {

    // for tv's use these filters: refresh rate, brand, resolution
    // else just brand
    if (searchFacet.displayName === 'Brand' || searchFacet.displayName === 'Refresh Rate' || searchFacet.displayName === 'Vertical Resolution' || searchFacet.displayName === "3D Technology") {

        var searchGroup = {
            input: {
                values: [],
                name: addFacetName(searchFacet.name)
            },
            groupText: searchFacet.displayName,
            options: []
        };

        searchFacet.options.forEach(option => {
            searchGroup.options.push({
                text: `${option.name} (${option.count})`,
                value: addFacetName(option.name)
            });
        });

        searchGroups.push(searchGroup);
    }
}

function addFacetName(str) {
    if (str.indexOf(':') === -1) {
        return str;
    }
    return str.split(':')[1];
}