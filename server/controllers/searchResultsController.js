var searchResultsController = module.exports;
var matchingService = require('../services/matchingService');
var storeService = require('../services/storeService');
var availabilityService = require('../services/availabilityService');
var defaultSearchResults = require('../models/defaultSearchResults');
var defaultAvailabilities = require('../models/defaultAvailabilities');
var defaultHome = require('../models/defaultHome');
var productHelper = require('./productHelper');
var mongoose = require('mongoose');
var Task = require('../models/Task');
var Guid = require('guid');
var config = require('../config/config');
var _ = require('lodash');

searchResultsController.index = function (req, res, next) {

    // validate required fields and return an invalid home resource in not valid
    if (!req.query.zipCode) {
        var model = defaultHome.model();
        model.zipCode.value = undefined;
        model.zipCode.textState = 'invalid';
        res.status(400).render('default', model);
        return;
    }

    if (!req.query.sku) {
        var model = defaultHome.model();
        model.sku.value = undefined;
        model.sku.textState = 'invalid';
        res.status(400).render('default', model);
        return;
    }

    // handle this up front
    var originalPurchasePrice = 'NA';
    if (req.query.originalPurchasePrice) {
        originalPurchasePrice = Number(req.query.originalPurchasePrice).toFixed(2);
    }

    // some basic dataModel stuff...
    var dataModel = defaultSearchResults.model();
    dataModel.zipCode = req.query.zipCode;
    dataModel.originalProduct.sku = req.query.sku;
    dataModel.originalProduct.originalPrice = originalPurchasePrice;

    dataModel.inputs = [
        { name: 'sku', value: req.query.sku },
        { name: 'zipCode', value: req.query.zipCode },
        { name: 'originalPurchasePrice', value: req.query.originalPurchasePrice }
    ];


    // uncomment if we want to really do faceted search...
    //if (req.query.brand) {
    //    dataModel.inputs.push({ name: 'brand', value: req.query.brand });
    //}
    //
    //if (req.query.refresh_rate) {
    //    dataModel.inputs.push({ name: 'refresh_rate', value: req.query.refresh_rate });
    //}
    //
    //if (req.query.vertical_resolution) {
    //    dataModel.inputs.push({ name: 'vertical_resolution', value: req.query.vertical_resolution });
    //}

    dataModel.urls.searchResults.refine.index = `/search-results/refine/${req.query.sku}`;


    return matchingService.match(req.query.sku, req.query)
        .then((result) => {

            if (!result.sku) {
                throw new Error(`${req.query.sku} is not a valid sku, or not a sku in the system`);
            }

            // map original product to the data model
            productHelper.map(result.sku, dataModel.originalProduct);

            return Promise.map(result.similarSkus, (product) => {
                // map attributes
                return mapProductAttributes(req, product);
            });
        })
        .then((products) => {

            // limit resultset to top 5
            dataModel.matchingProducts = _.slice(products, 0, 5);

            // return response
            res.render('./search-results/default', dataModel);
        })
        .catch((err) => {
            log.error(err.message);
            var model = defaultHome.model();
            model.sku.textState = 'invalid';
            res.status(400).render('default', model);
        });
};


searchResultsController.checkAvailability = function (req, res, next) {

    // generate a known ID so we can track the task in-between requests
    var taskId = Guid.raw();

    // create a task
    var fulfillmentTask = new Task({
            taskId: taskId,
            message: `Checking fulfillment - Task ID: ${taskId}`
        }
    );

    var stores;
    storeService.closestStoresByZipcode(req.query.zipCode)

        // not sure which store matters, just use the first store
        .then(_stores => {
            stores = _stores;
            //return availabilityService.getFulfillmentOptions(req.query.sku, stores[0]);
            return matchingService.match(req.query.sku);
        })
        .then(selectedProduct => {

            // product owner wants to ignore oms and do their own fulfillment logic ¯\_(ツ)_/¯
            if (selectedProduct.sku.class.id == '140') {
                return getPickupAvailability(stores, req.query.sku, req.query.zipCode)
            }

            return availabilityService.getDeliveryAvailability(req.query.sku, req.query.zipCode, stores[0])
                .then(deliveryAvailability => {
                    if (deliveryAvailability.sku) {
                        return defaultAvailabilities.del_available;
                    }
                    return defaultAvailabilities.del_unavailable;
                });
        })
        .then(result => {
            fulfillmentTask.result = result;
            return fulfillmentTask.save()
        })
        .catch((err) => {
            log.error(err);
        });

    res.render('./search-results/availability/default', createPendingModel(taskId));
};

searchResultsController.checkAvailabilityStatus = function (req, res, next) {

    Task.findOne({taskId: req.params.id}, function (error, task) {
        if (error) {
            log.error(error);
        }

        // task may not be done yet
        if (!task) {
            res.render('./search-results/availability/default', createPendingModel(req.params.id));
        }
        else {
            res.render('./search-results/availability/default', task.result);
        }
    });
};


function getPickupAvailability(stores, sku, zipCode) {
    return Promise.map(stores, (store) => {
            return availabilityService.getStorePickupAvailability(sku, zipCode, store)
                .then((result) => {
                    if (result.fulfillmentDate) {
                        var availabilityResult = defaultAvailabilities.spu_available;
                        availabilityResult.availabilityMessage = `Availabile on ${result.fulfillmentDate}`;
                        return availabilityResult;
                    }
                    return defaultAvailabilities.spu_unavailable;
                });
        })
        .filter(result => result.pending == false)
        .then(results => results[0]);
}

// helper function to map attributes (product + lynx urls)
function mapProductAttributes(req, product) {

    return productHelper.map(product, {
        "urls": {
            "comparison": `/comparison?originalSku=${req.query.sku}&similarSku=${product.skuId}&zipCode=${req.query.zipCode}&originalPurchasePrice=${req.query.originalPurchasePrice}`,
            "searchResults": {
                "availability": {
                    "index": `/search-results/availability?sku=${product.skuId}&zipCode=${req.query.zipCode}`
                }
            }
        }
    });
}

function createPendingModel(taskId) {
    return {
        pending: true,
        urls: {
            searchResults: {
                availability: {
                    index: `/search-results/availability/status/${taskId}`
                }
            }
        }
    };
}