var defaultHome = require('../models/defaultHome');

module.exports.index = function (req, res) {
    res.render('default', defaultHome.model());
};