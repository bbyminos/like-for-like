var matchingService = require('../services/matchingService');
var defaultComparison = require('../models/defaultComparison');
var productHelper = require('./productHelper');
var _ = require('lodash');

var defaulted = 'NA';

module.exports.index = function (req, res) {

    var dataModel = defaultComparison.model();
    dataModel.urls.searchResults.index = `/search-results?sku=${req.query.originalSku}&zipCode=${req.query.zipCode}&originalPurchasePrice=${req.query.originalPurchasePrice}`

    return Promise.join(matchingService.match(req.query.originalSku), matchingService.match(req.query.similarSku))
        .spread((originalResult, similarResult) => {

            // handle results here
            productHelper.map(originalResult.sku, dataModel.results[0].original);
            productHelper.map(similarResult.sku, dataModel.results[0].comparison);

            log.info("comparing these attributes - ");
            log.info(dataModel.results[0].original.comparableAttributes);
            log.info(dataModel.results[0].comparison.comparableAttributes);

            for (i = 0; i < dataModel.results[0].comparison.comparableAttributes.length; i++) {
                var obj = {};
                _.set(obj, 'header', dataModel.results[0].comparison.comparableAttributes[i].label);
                _.set(obj, 'original', findAttributeOrDefault(dataModel.results[0].original.comparableAttributes, dataModel.results[0].comparison.comparableAttributes[i].label));
                _.set(obj, 'comparison', _.get(dataModel.results[0].comparison.comparableAttributes[i], 'text', defaulted));
                dataModel.results.push(obj);
            }

            // set the original purchase price received on the original search form
            var originalPrice = "NA";
            if (req.query.originalPurchasePrice) {
                originalPrice = req.query.originalPurchasePrice
            }
            dataModel.results[0].original.originalPrice = originalPrice;

            res.render('./comparison/default', dataModel);
        });
};

function findAttributeOrDefault(source, attr) {
    var val = _.find(source, { label: attr });
    if(!val) {
        return 'NA';
    }
    return val.text;
}

