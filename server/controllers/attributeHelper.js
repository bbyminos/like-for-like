// Ordinals work by querying the product document
//  there are different ordinal numbers for the original vs. matching skus.
//  All ordinals should return a displayName and values, but sku vs. similarSku may not
//   have all ordinals.
//
//  sku.specifications": [
//{
//    "sequence": 0,
//    "displayName": "Energy Star Compatible",
//    "tags": [
//    "displayable"
//],
//    "values": [
//    "true"
//]
//}
//
//
//  similarSku.specifications: [
//{
//    "sequence": 2,
//    "rawName": "Audio_and_Video:Manufacturers_Warranty_Parts",
//    "displayName": "Manufacturer's Warranty - Parts",
//    "tags": [
//    "displayable",
//    "facttag",
//    "specification attribute"
//],
//    "values": [
//    "1 year limited"
//]
//},


//  Color, Display Type, Refresh Rate, Number Of HDMI Inputs,
//  Number of USB Port(s), Screen Size Class, Vertical Resolution,
//  PC Input, Number of DVI Inputs
module.exports.televisionAttributes = [
    'Display Type',   //display type
    'Refresh Rate',   // refresh rate
    'Number of HDMI Inputs',   // number hdmi inputs
    'Number of USB Port(s)',   // number usb ports
    'Screen Size Class',   // screen size class
    'Vertical Resolution',   // vertical resolution
    'PC Inputs',   // pc input
    'Smart Capable',
    '3D Technology',   // smart capable
    'Curved Screen'
];


// Brand, Model Number, SKU, Operating System, Hard Drive Capacity, Hard Drive Type, Processor Speed,
// Processor Model, System Memory (RAM), Screen Size, Touch Screen, Number of HDMI Inputs, Number of USB Ports
module.exports.laptopAttributes = [
    'Hard Drive Capacity',
    'Hard Drive Type',
    'Number Of HDMI Inputs',
    'Number Of Port(s)',
    'Operating System',
    'Processor Speed',
    'Processor Model',
    'System Memory',
    'Screen Size Class',
    'Touch Screen'
];


module.exports.defaultAttributes = [

];









