var expect = require('expect');
var log = require('../../server/config/log');
var searchResultsController = require('./searchResultsController');
var matchingService = require('../services/matchingService');
var Promise = global.Promise = require('bluebird');
Promise.promisifyAll(require('request'));

var knownSku = 1725534;

//describe('filter criteria >', function() {
//
//    describe('filter >', function () {
//
//        //it('build basic searchGroups', function (done) {
//        //    matchingService.match(knownSku)
//        //        .then(matchingServiceResult => {
//        //
//        //            var result = searchResultsController.createSearchCriteria(matchingServiceResult.facets);
//        //            log.info(JSON.stringify(result));
//        //            expect(result.searchGroups).toExist();
//        //            expect(result.searchGroups[0].input.name).toBeA('string');
//        //            expect(result.searchGroups[0].groupText).toBeA('string');
//        //            expect(result.searchGroups.length).toBe(3);
//        //            done();
//        //        })
//        //});
//    });
//});




//{
//    "searchGroups": [
//    {
//        "input": {
//            "values": [],
//            "name": "aspect-ratio"
//        },
//        "groupText": "Aspect Ratio",
//        "options": [
//            {
//                "text": "16:9 (82)",
//                "value": "16:9"
//            },
//            {
//                "text": "4:3 (22)",
//                "value": "4:3"
//            }
//        ]
//    },
//    {
//        "input": {
//            "values": [],
//            "name": "brand"
//        },
//        "groupText": "Brand",
//        "options": [
//            {
//                "text": "Samsung (12)",
//                "value": "Samsung"
//            },
//            {
//                "text": "Sony (8)",
//                "value": "Sony"
//            }
//        ]
//    },
//    {
//        "input": {
//            "values": [],
//            "name": "color"
//        },
//        "groupText": "Color",
//        "options": [
//            {
//                "text": "Black (32)",
//                "value": "Black"
//            },
//            {
//                "text": "Silver (8)",
//                "value": "Silver"
//            },
//            {
//                "text": "Grey (1)",
//                "value": "Grey"
//            }
//        ]
//    },
//    {
//        "input": {
//            "values": [],
//            "name": "resolution"
//        },
//        "groupText": "Resolution",
//        "options": [
//            {
//                "text": "2160p/4K (43)",
//                "value": "2160p"
//            },
//            {
//                "text": "1080p (32)",
//                "value": "1080p"
//            },
//            {
//                "text": "720p (4)",
//                "value": "720p"
//            }
//        ]
//    },
//    {
//        "input": {
//            "values": [],
//            "name": "tvtype"
//        },
//        "groupText": "TV Type",
//        "options": [
//            {
//                "text": "LED (64)",
//                "value": "led"
//            },
//            {
//                "text": "Smart (56)",
//                "value": "smart"
//            },
//            {
//                "text": "4K UHD (43)",
//                "value": "4kuhd"
//            },
//            {
//                "text": "Curved (14)",
//                "value": "curved"
//            },
//            {
//                "text": "HDR (22)",
//                "value": "hdr"
//            },
//            {
//                "text": "3D (7)",
//                "value": "3d"
//            }
//        ]
//    },
//    {
//        "input": {
//            "values": [],
//            "name": "inputs-outputs"
//        },
//        "groupText": "Inputs/Outputs",
//        "options": [
//            {
//                "text": "USB Port(s) (64)",
//                "value": "USB Port(s)"
//            },
//            {
//                "text": "HDMI Input(s) (59)",
//                "value": "HDMI Input(s)"
//            },
//            {
//                "text": "Component Video Input(s) (35)",
//                "value": "Component Video Input(s)"
//            },
//            {
//                "text": "Digital Optical Audio Output(s) (34)",
//                "value": "Digital Optical Audio Output(s)"
//            },
//            {
//                "text": "Analog Audio Output(s) (8)",
//                "value": "Analog Audio Output(s)"
//            },
//            {
//                "text": "HDMI Output(s) (8)",
//                "value": "HDMI Output(s)"
//            },
//            {
//                "text": "Headphone Jack (3)",
//                "value": "Headphone Jack"
//            }
//        ]
//    },
//    {
//        "input": {
//            "values": [],
//            "name": "hdmi-inputs"
//        },
//        "groupText": "Number of HDMI Inputs",
//        "options": [
//            {
//                "text": "4 (25)",
//                "value": 4
//            },
//            {
//                "text": "3 (15)",
//                "value": 3
//            },
//            {
//                "text": "2 (19)",
//                "value": 2
//            }
//        ]
//    },
//    {
//        "input": {
//            "values": [],
//            "name": "customer-rating"
//        },
//        "groupText": "Customer Rating",
//        "options": [
//            {
//                "text": "Top-Rated (50)",
//                "value": "Top-Rated"
//            },
//            {
//                "text": "5 (5)",
//                "value": 5
//            },
//            {
//                "text": "4 & Up (63)",
//                "value": "4 & Up"
//            },
//            {
//                "text": "3 & Up (63)",
//                "value": "3 & Up"
//            },
//            {
//                "text": "2 & Up (63)",
//                "value": "2 & Up"
//            },
//            {
//                "text": "1 & Up (63)",
//                "value": "1 & Up"
//            }
//        ]
//    }
//]
//}