exports.config = {
    allScriptsTimeout: 11000,
    specs: [
        '*.spec.js'
    ],
    capabilities: {
        'browserName': 'chrome'
    },
    baseUrl: 'http://localhost:3001/client?url=/',
    framework: 'jasmine2',
    jasmineNodeOpts: {
        defaultTimeoutInterval: process.env.PROTRACTOR_DEBUG ? 9999999 : 30000
    },
    onPrepare: function () {
        browser.driver.manage().window().setSize(1280, 1024);
        return browser.driver.get(browser.baseUrl);
    }
};