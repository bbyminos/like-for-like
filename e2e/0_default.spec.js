describe('default >', () => {

    beforeAll(() => {
        browser.driver.get('/client?url=/');
    });

    it('can enter a zip code', () => {
        var locations = element.all(by.css('.list-group-item'));
        expect(locations.count()).toEqual(1);
        element(by.buttonText('Corporate')).click();
        expect(browser.driver.getCurrentUrl()).toMatch('/role');
    });

    it('can select a role', () => {
        element.all(by.css('.list-group-item')).first().click();
        expect(browser.driver.getCurrentUrl()).toMatch('/');
        expect(browser.driver.isElementPresent(by.buttonText('Start survey'))).toEqual(false);
    });
})